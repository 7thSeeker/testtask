﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using TestTaskLib;
using WebSocketSharp;
using WebSocketSharp.Server;
using System.IO;

namespace LoginServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("======== LOGIN SERVER ========\n");

           
            string address = string.Format("ws://{0}", Strings.localAddress);
           
            Console.WriteLine("address: " + address);

            IPHostEntry he = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ips in he.AddressList)
            {
                Console.WriteLine("local: " + ips);
            }
            var wssv = new WebSocketServer(address);
            wssv.AddWebSocketService<LoginServer>("/LoginServer");
            wssv.Start();
            Console.ReadKey(true);

            wssv.Stop();
        }
    }

    public class LoginServer : WebSocketBehavior
    {
        public static string gameServerUrl { get; private set; }
        public static string gameServerSessionID { get; private set; }
        public static GameClientsMatchups matchups =  new GameClientsMatchups();
        public static Dictionary<string, string> clientSessionIDs = new Dictionary<string, string>();

        static int clientsCounter;

        protected override void OnOpen()
        {
            Console.WriteLine("Login server started");
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            //base.OnMessage(e);
            if (e.IsBinary)
            {
                Message msg = (Message)Binaries.ByteArrayToObject(e.RawData);

                switch (msg.senderTag)
                {
                    case "GameServer":

                        if (msg.command == "GameServerInitialize")
                        {
                            gameServerUrl = string.Format("ws://{0}:{1}/GameServer", Context.Host, msg.args[0]);
                            gameServerSessionID = ID;
                            Console.WriteLine("GameServer connected \nID: {0}\nAddress: {1}\nURL: {2}\n", ID, Context.Host, gameServerUrl);
                        }
                        
                        else if (msg.command == "ClientDisconnected")
                        {
                            Console.WriteLine("GameServer says: session is over for " + msg.args[0]);
                            var sessionid = (string)msg.args[0];
                            Sessions.SendTo("LoginServer says: Your session is over.", sessionid);
                        }
                                              
                        break;

                    case "Client":

                        if (msg.command == "Connect")
                        {
                            Console.WriteLine("Client connected. ID: " + ID + " Address: " + Context.Host);

                            if (gameServerUrl == string.Empty)
                            {
                                Console.WriteLine("No game server active. Session will be terminated\n");
                                Sessions.CloseSession(ID);
                            }

                            string clientID = /*ID;*/"Client_" + ++clientsCounter;
                            clientSessionIDs.Add(clientID, ID);

                            var pair = matchups.FindMatchup(clientID);

                            Console.WriteLine("Joined to: " + pair.hostSessionID);

                            while (!pair.isReadyToGame)
                            {
                                //Console.WriteLine(ID + "is awaiting for an opponent");
                                Thread.Sleep(1000);
                                Send("Wait for an opponent...");
                            }

                            Console.WriteLine(clientID + " found his opponent! Redirect to GameServer.");
                            Send("Opponent found");

                            if (pair.hostSessionID == clientID)
                            {
                                Console.WriteLine("HostSessionID: " + pair.hostSessionID);

                                Message newMsg = new Message("LoginServer", "NewMatch");
                                newMsg.AddArgument(pair);

                                var binMsg = Binaries.ObjectToByteArray(newMsg);

                                Sessions.SendTo(binMsg, gameServerSessionID);
                            
                            }


                            var message = new Message("LoginServer", "LaunchGame");
                            message.AddArgument(gameServerUrl);
                            message.AddArgument(clientID);
                            message.AddArgument(pair.matchID);
                            message.AddArgument(ID);
                            var response = Binaries.ObjectToByteArray(message);

                            Send(response);
                        }

                        break;

                    default:
                        Console.WriteLine("Received message from an source unknown. Session will be terminated. ID: {0}\n", ID);
                        Sessions.CloseSession(ID);
                        break;
                }
            }
        }

    }
}
