﻿using System;
using System.Threading;
using TestTaskLib;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace GameServer
{
    class Program
    {
        public static WebSocket loginServerSocket;
        static void Main(string[] args)
        {
            Console.WriteLine("======== GAME SERVER ========\n");

            Thread.Sleep(1000);

            int port = Strings.gsPort;
            string external = Strings.externalAddress;

            loginServerSocket = new WebSocket(string.Format("ws://{0}/LoginServer", external));
            Console.WriteLine("GameServer launched, trying to connect to LoginServer");
            loginServerSocket.OnOpen += (sender, e) => Console.WriteLine("LoginServer connection established");
            loginServerSocket.OnMessage += clientSocket_OnMessage;
            loginServerSocket.Connect();

            var serverSocket = new WebSocketServer(port);
            serverSocket.AddWebSocketService<GameServer>("/GameServer");
            Console.WriteLine("Initialize GameServer services...");
            serverSocket.Start();
            Console.WriteLine("GameServer successfully started");

            Message message = new Message("GameServer", "GameServerInitialize");
            message.AddArgument(port.ToString());
            var binaryData = Binaries.ObjectToByteArray(message);
            loginServerSocket.Send(binaryData);

            Console.ReadKey();
        }

        private static void clientSocket_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                var msg = (Message)Binaries.ByteArrayToObject(e.RawData);

                switch (msg.senderTag)
                {
                    case "LoginServer":

                        if (msg.command == "NewMatch")
                        {
                            var matchup = (ClientsPair)msg.args[0];

                            AddClientsPair(matchup);

                            matchup.EraseOldIDs();
                        }

                        break;
                    default:
                        Console.WriteLine("Unknown message source: " + msg.senderTag);
                        break;
                }

            }
            else
                Console.WriteLine("client socket data: " + e.Data);
        }

        public static GameClientsMatchups matchups = new GameClientsMatchups();

        public static void AddClientsPair(ClientsPair pair)
        {
            matchups.AddNew(pair);
        }

    }

    public class GameServer : WebSocketBehavior
    {
        string loginSessionID;
        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                var msg = (Message)Binaries.ByteArrayToObject(e.RawData);

                switch (msg.senderTag)
                {
                    case "GameClient":
                        if (msg.command == "NewClient")
                        {
                            loginSessionID = (string)msg.args[1];

                            var messageToLogin = new Message("GameServer", "GameSessionClientID");
                            messageToLogin.AddArgument(ID);
                            
                            var b = Binaries.ObjectToByteArray(messageToLogin);
                            Program.loginServerSocket.Send(b);

                            Console.WriteLine(msg.command + msg.args[0]);
                            //var match = Program.matchups.GetPairByID((string)msg.args[0]);

                            ClientsPair match = null;

                            while (match == null)
                            {
                                match = Program.matchups.GetPairByID((string)msg.args[0]);
                                Thread.Sleep(1000);
                            }

                            match.AddImmediate(ID);

                            while (!match.isReadyToGame)
                            {
                                Thread.Sleep(1000);

                            }
                            //Console.WriteLine("Game ready");
                            var _msg = new Message("GameServer", "GameReady");
                            var response = Binaries.ObjectToByteArray(_msg);
                            Send(response);
                        }

                        else if (msg.command == "Message")
                        {
                            var match = Program.matchups.FindPairByClientSessionID(ID);

                            for(int i = 0; i < match.ids.Count; i++)
                            {
                                Console.WriteLine("MatchID: '{0}' To: Client#{1} Message: {2}", match.matchID, i, msg.args[0].ToString());
                                //Sessions.SendTo(msg.args[0].ToString(), match.ids[i]);
                                var message = new Message("GameServer", "Message");
                                message.AddArgument(msg.args[0]);
                                message.AddArgument(msg.args[1]);
                                var bin = Binaries.ObjectToByteArray(message);

                                Sessions.SendToAsync(bin, match.ids[i], null);
                            }

                        }

                        else if (msg.command == "ClientDisconnected")
                        {
                            Console.WriteLine("Session is over for: " + ID);
                            var resp = new Message("GameServer", "ClientDisconnected");
                            resp.AddArgument(loginSessionID);
                            Program.loginServerSocket.Send(Binaries.ObjectToByteArray(resp));                     
                        }

                        break;

                    default:
                        Console.WriteLine("Unknown source: " + msg.senderTag);
                        break;
                }
            }
            else
                Console.WriteLine("Received data: " + e.Data);
        }
    }

}
