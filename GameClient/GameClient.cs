﻿using System;
using System.Threading;
using TestTaskLib;
using WebSocketSharp;
using System.Diagnostics;

namespace GameClient
{
    class Program
    {
        static WebSocket loginServerClient;
        static WebSocket gameServerClient;
        static string name;
        //static string loginServerSessionID;

        static void Main(string[] args)
        {
            Console.WriteLine("======== GAME CLIENT ========\n");
            Thread.Sleep(2000);

            string external = Strings.externalAddress;
            loginServerClient = new WebSocket(string.Format("ws://{0}/LoginServer", external));

            loginServerClient.OnOpen += (sender, e) => loginServerClient.Send(Binaries.ObjectToByteArray(new Message("Client", "Connect")));
            loginServerClient.OnOpen += (sender, e) => { Console.WriteLine("LoginServer connection established"); };

            loginServerClient.OnMessage += loginServerClient_OnMessage;      

            loginServerClient.Connect();
            
            Console.ReadKey();
        }


        static void loginServerClient_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                var response = (Message)Binaries.ByteArrayToObject(e.RawData);

                switch (response.command)
                {
                    case "LaunchGame":

                        //loginServerSessionID = (string)response.args[3];
                        var url = (string)response.args[0];
                        Console.WriteLine("LaunchGame command received. \nOpening new socket: {0}\n", url);
                        name =  (string)response.args[1];
                        gameServerClient = new WebSocket(url);
                        gameServerClient.OnMessage += gameServerClient_OnMessage;
                        gameServerClient.Connect();

                        var message = new Message("GameClient", "NewClient");
                        message.AddArgument(response.args[2]);
                        message.AddArgument(response.args[3]);

                        var bytes = Binaries.ObjectToByteArray(message);

                        gameServerClient.Send(bytes);

                        break;

                    default:
                        Console.WriteLine("Unknown command: " + response.command);

                        break;
                }
            }
            else
                Console.WriteLine(e.Data);
        }

        static void SendMessages()
        {
            var rnd = new Random();
            Message msg;
            byte[] data;

            var stopwatch = new Stopwatch();
            float timer = 60000;

            Console.WriteLine("Session expires in {0} seconds", timer / 1000);
            stopwatch.Start();
            while (true)
            {
                //Console.WriteLine("Sending data");



                if (stopwatch.ElapsedMilliseconds >= timer)
                {
                    stopwatch.Stop();
                    break;
                }

                msg = new Message("GameClient", "Message");
                msg.AddArgument(rnd.Next());
                msg.AddArgument(name);
                data = Binaries.ObjectToByteArray(msg);

                gameServerClient.SendAsync(data, null);

                Thread.Sleep(1000);
            }

            Console.WriteLine("Session expired. Connection to server will be closed after a while.");

            var disconnectMessage = Binaries.ObjectToByteArray(new Message("GameClient", "ClientDisconnected"));

            gameServerClient.Send(disconnectMessage);

            gameServerClient.Close();
        }

        static void gameServerClient_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                var response = (Message)Binaries.ByteArrayToObject(e.RawData);

                switch (response.command)
                {

                    case "GameReady":

                        Thread t = new Thread(SendMessages);
                        t.IsBackground = true;
                        t.Start();

                        break;

                    case "Message":

                        Console.WriteLine("[{0}]: {1}", response.args[1], response.args[0]);

                        break;

                    default:

                        Console.WriteLine("Unknown command: " + response.command);

                        break;

                }
            }
            else
                Console.WriteLine("Some unknown data received: " + e.Data);
        }
    }
}
