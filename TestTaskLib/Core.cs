﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace TestTaskLib
{
    public class Strings
    {
        public static string[] getAllCfg
        { 
            get 
            { 
                return File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "config.txt"); 
            } 
        }

        public static string localAddress
        {
            get
            {
                return getAllCfg[0].Between("local_ip=", ";");
            }
        }

        public static string externalAddress
        {
            get
            {
                return getAllCfg[1].Between("external_ip=", ";");
            }
        }

        public static int gsPort
        {
            get
            {
                return int.Parse(getAllCfg[2].Between("gameserver_port=", ";"));
            }
        }
    }
    public static class Extensions
    {
        public static string Between(this string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }
    }
    public class Binaries
    {
        public static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;

            var bf = new BinaryFormatter();
            var ms = new MemoryStream();

            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static Object ByteArrayToObject(byte[] buffer)
        {
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();

            ms.Write(buffer, 0, buffer.Length);
            ms.Seek(0, SeekOrigin.Begin);
            var obj = bf.Deserialize(ms);

            return obj;
        }
    }

    [Serializable]
    public class ClientsPair
    {

        const int maxplayers = 2;
        public List<string> ids { get; private set; }
        public string matchID { get; private set; }
        public ClientsPair(string matchID)
        {
            ids = new List<string>();
            this.matchID = matchID;
        }

        public void EraseOldIDs()
        {
            Console.WriteLine("Erase old its at : " + matchID);
            ids = new List<string>();
        }

        /// <summary>
        /// Use this carefully because it ignores maxplayers and may cause unexpected errors
        /// </summary>
        /// <param name="id"></param>
        public void AddImmediate(string id)
        {
            ids.Add(id);
        }

        public bool TryAdd(string id)
        {
            Console.WriteLine("Attempt add {0} to {1}", id, hostSessionID);
            if (ids.Count < maxplayers)
            {
                ids.Add(id);
                return true;
            }
            else
                return false;
        }

        public bool isReadyToGame
        {
            get { return ids.Count == maxplayers; }
        }

        public string hostSessionID
        {
            get { return ids.Count > 0 ? ids[0] : "as new host"; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            ClientsPair comparedPair = (ClientsPair)obj;

            if (comparedPair == null || ids.Count != comparedPair.ids.Count)
                return false;

            for (int i = 0; i < ids.Count; i++)
            {
                if (ids[i] != comparedPair.ids[i])
                    return false;
            }

            return true;
        }
        public override int GetHashCode()
        {
            int sum = 0;
            for (int i = 0; i < 0; i++)
            {
                sum += ids[i].GetHashCode() * (i * 17 + 1);
            }
            return sum;
        }
    }
    [Serializable]
    public class GameClientsMatchups
    {
        public List<ClientsPair> pairs { get; private set; }

        public GameClientsMatchups()
        {
            pairs = new List<ClientsPair>();
        }

        public ClientsPair FindPairByClientSessionID(string id)
        {
            return pairs.First(x => x.ids.Contains(id));
        }

        public void DestroyMatchByMatchID(string id)
        {
            pairs.Remove(pairs.First(x => (x.matchID == id)));
        }

        public void DestroyMatchBySessionID(string id)
        {
            pairs.Remove(pairs.First(x => (x.hostSessionID == id)));
        }

        public ClientsPair GetPairByID(string id)
        {
            Console.WriteLine("Key id: " + id);
            Console.WriteLine("Count: " + pairs.Count);
            pairs.ForEach(y => Console.WriteLine("available: " + y.matchID));

            if (!pairs.Any(x => x.matchID == id))
            {
                return null;
            }

            return pairs.First(x => x.matchID == id);
        }

        public ClientsPair FindMatchup(string id)
        {
            Console.WriteLine("Queue length: " + pairs.Count);
            for (int i = 0; i < pairs.Count; i++)
            {
                if (pairs[i].TryAdd(id))
                {
                    Console.WriteLine("Found free empty slot for " + id);
                    return pairs[i];
                }
            }

            Console.WriteLine("No free slots founded, create new match. ID: " + id);
            var newMatch = new ClientsPair(id);
            newMatch.TryAdd(id);
            pairs.Add(newMatch);
            return newMatch;
        }

        public void AddNew(ClientsPair pair)
        {
            pairs.Add(pair);
        }
    }
    [Serializable]
    public class Message
    {
        public string senderTag { get; private set; }
        public string command { get; private set; }
        public List<object> args { get; private set; }

        public Message(string senderTag, string command)
        {
            this.senderTag = senderTag;
            this.command = command;
            args = new List<object>();
        }

        public void AddArgument(object arg)
        {
            args.Add(arg);
        }
    }

    //public struct GameServerInfo
    //{
    //    public string address;
    //    public int port;

    //    public GameServerInfo(string address, int port)
    //    {
    //        this.address = address;
    //        this.port = port;
    //    }
    //}

    //public class TestTaskBaseClass
    //{
    //    public static void Log(object obj)
    //    {
    //        string date = DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss");
    //        Console.WriteLine(string.Format("[{0}] {1}", date, obj.ToString()));
    //    }


    //}

    //[Serializable]
    //public struct ClientResponse
    //{
    //    public ClientType type;

    //    public ClientResponse(ClientType type, string address)
    //    {
    //        this.type = type;
    //    }

    //    public override bool Equals(object obj)
    //    {
    //        if (obj == null)
    //            return false;

    //        var comparingClient = (ClientResponse)obj;

    //        return this.type == comparingClient.type;
    //    }
    //    public override int GetHashCode()
    //    {
    //        return type.GetHashCode();
    //    }
    //    public override string ToString()
    //    {
    //        return string.Format("Client: {0}", type);
    //    }
    //}
    //public enum ClientType
    //{
    //    GameServer,
    //    GameClient,
    //}
}
